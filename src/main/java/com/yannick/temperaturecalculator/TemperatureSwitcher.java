package com.yannick.temperaturecalculator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class TemperatureSwitcher extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public double getTemperature(String temp) {
        try {
            return (Double.parseDouble(temp) * 1.8) + 32;
        } catch (NumberFormatException e) {
            return 0.0;
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature Switcher";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature (Celcius): " +
                request.getParameter("temp") + "\n" +
                "  <P>Temperature (Fahrenheit): " +
                getTemperature(request.getParameter("temp")) +
                "</BODY></HTML>");
    }
}
